package com.company;


import com.company.cache.Cache;
import com.company.cache.CacheEntry;
import com.company.cache.DefaultCache;
import com.company.protocol.Protocol;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import static com.company.protocol.Protocol.bufferOf;
import static com.company.protocol.Protocol.stringOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anySet;
import static org.mockito.Mockito.times;

/**
 * Test methods for SessionTest.
 */
public class SessionTest {
  private static final Cache CACHE = Mockito.spy(new DefaultCache());
  private static final TestSession SESSION;

  static {
    TestSession session;
    try {
      session = Mockito.spy(new TestSession(CACHE));
    } catch (IOException e) {
      session = null;
      fail();
    }
    SESSION = session;
  }

  @Before
  public void reset() {
    CACHE.clear();
    Mockito.reset(CACHE, SESSION);
  }

  @After
  public void clearResponses()
      throws IOException {
    SESSION.processResponses();
  }

  @Test
  public void testRequestQueue()
      throws IOException {
    SESSION.queueRequest(bufferOf("one\r\n"));
    assertEquals(1, SESSION.requestQueueSize());
    assertEquals("one", Protocol.stringOf(SESSION.readRequestLine()));
    assertEquals(0, SESSION.requestQueueSize());
  }

  @Test
  public void testMultipleQueuedRequests()
      throws IOException {
    SESSION.queueRequest(bufferOf("one\r\ntwo\r\nthree\r\n"));
    assertEquals(1, SESSION.requestQueueSize());
    assertEquals("one", Protocol.stringOf(SESSION.readRequestLine()));
    assertEquals("two", Protocol.stringOf(SESSION.readRequestLine()));
    assertEquals("three", Protocol.stringOf(SESSION.readRequestLine()));
    assertEquals(0, SESSION.requestQueueSize());
  }

  @Test
  public void testSpanningRequest()
      throws IOException {
    SESSION.queueRequest(bufferOf("one"));
    SESSION.queueRequest(bufferOf("two"));
    SESSION.queueRequest(bufferOf("three"));
    SESSION.queueRequest(bufferOf("\r\n"));
    assertEquals(4, SESSION.requestQueueSize());
    assertEquals("onetwothree", Protocol.stringOf(SESSION.readRequestLine()));
  }

  @Test
  public void testSpanningRequest2()
      throws IOException {
    SESSION.queueRequest(bufferOf("one"));
    SESSION.queueRequest(bufferOf("two\r\n"));
    SESSION.queueRequest(bufferOf("three\r\nfour\r\n"));
    assertEquals(3, SESSION.requestQueueSize());
    assertEquals("onetwo", Protocol.stringOf(SESSION.readRequestLine()));
    assertEquals("three", Protocol.stringOf(SESSION.readRequestLine()));
    assertEquals("four", Protocol.stringOf(SESSION.readRequestLine()));
    assertEquals(0, SESSION.requestQueueSize());
  }

  @Test
  public void testRequestIterator()
      throws IOException {
    SESSION.queueRequest(bufferOf("one\r\n"));
    SESSION.queueRequest(bufferOf("two\r\n"));
    SESSION.queueRequest(bufferOf("three\r\n"));
    SESSION.queueRequest(bufferOf("four\r\nfive\r\n"));
    assertTrue(SESSION.hasNextRequestBuffer());

    assertEquals("one", Protocol.stringOf(SESSION.nextRequestBuffer()));
    assertTrue(SESSION.hasNextRequestBuffer());
    assertEquals("two", Protocol.stringOf(SESSION.nextRequestBuffer()));
    assertTrue(SESSION.hasNextRequestBuffer());
    assertEquals("three", Protocol.stringOf(SESSION.nextRequestBuffer()));
    assertTrue(SESSION.hasNextRequestBuffer());
    assertEquals("four", Protocol.stringOf(SESSION.nextRequestBuffer()));
    assertTrue(SESSION.hasNextRequestBuffer());
    assertEquals("five", Protocol.stringOf(SESSION.nextRequestBuffer()));
    assertFalse(SESSION.hasNextRequestBuffer());
  }

  @Test
  public void testSegmentedRequest()
      throws IOException {
    SESSION.queueRequest(bufferOf("get keyone "));
    SESSION.processRequests();
    Mockito.verify(CACHE, times(0)).get(anySet());
    Mockito.verify(SESSION, times(0)).onResponse(any(ByteBuffer[].class));

    SESSION.queueRequest(bufferOf("keytwo "));
    SESSION.processRequests();
    Mockito.verify(CACHE, times(0)).get(anySet());
    Mockito.verify(SESSION, times(0)).onResponse(any(ByteBuffer[].class));

    SESSION.queueRequest(bufferOf("keythree\r\n"));
    SESSION.processRequests();
    Mockito.verify(CACHE, times(1)).get(anySet());
    Mockito.verify(SESSION, times(1)).onResponse(any(ByteBuffer[].class));
    ByteBuffer[] byteBuffers = SESSION.processResponses();
    // Nothing in the cache
    assertEquals(1, byteBuffers.length);
    assertEquals("END", Protocol.stringOf(byteBuffers[0]));
  }

  /**
   * Simulate a get request distributed over 2 bytebuffers, simulates the case
   * when using many keys in a get request
   */
  @Test
  public void testFragmentedGet()
      throws IOException {
    SESSION.queueRequest(bufferOf("get keyone "));
    SESSION.queueRequest(bufferOf("keytwo "));
    SESSION.queueRequest(bufferOf("keythree\r\n"));
    SESSION.processRequests();
    Mockito.verify(CACHE, times(1)).get(anySet());
    Mockito.verify(SESSION, times(1)).onResponse(any(ByteBuffer[].class));
    ByteBuffer[] byteBuffers = SESSION.processResponses();
    // Nothing in the cache
    assertEquals(1, byteBuffers.length);
    assertEquals("END", Protocol.stringOf(byteBuffers[0]));
  }

  /**
   * Simulate a get request distributed over 3 bytebuffers, simulates the case
   * when using many keys in a get request and in the final request buffer is
   * comprised of two different request lines
   */
  @Test
  public void testFragmentedGet2()
      throws IOException {
    SESSION.queueRequest(bufferOf("get keyone "));
    SESSION.queueRequest(bufferOf("keytwo "));
    SESSION.queueRequest(bufferOf("keythree\r\nget keyfour\r\n"));
    SESSION.processRequests();
    Mockito.verify(CACHE, times(2)).get(anySet());
    Mockito.verify(SESSION, times(2)).onResponse(any(ByteBuffer[].class));
    ByteBuffer[] byteBuffers = SESSION.processResponses();
    // Nothing in the cache
    assertEquals(2, byteBuffers.length);
    assertEquals("END", Protocol.stringOf(byteBuffers[0]));
    assertEquals("END", Protocol.stringOf(byteBuffers[1]));

  }

  /**
   * Simulate a get request distributed over 2 bytebuffers, simulates the case
   * when using many keys in a get request and in the final request buffer is
   * comprised of two different request lines
   */
  @Test
  public void testFragmentedGet3()
      throws IOException {
    SESSION.queueRequest(bufferOf("get keyone\r\nget keyzero keyfive "));
    SESSION.queueRequest(bufferOf("\r\nget keytwo\r\nget keysix\r\n"));
    SESSION.queueRequest(bufferOf("get keythree\r\nget keyfour\r\n"));
    SESSION.processRequests();
    Mockito.verify(CACHE, times(6)).get(anySet());
    Mockito.verify(SESSION, times(6)).onResponse(any(ByteBuffer[].class));
    ByteBuffer[] byteBuffers = SESSION.processResponses();
    // Nothing in the cache
    assertEquals(6, byteBuffers.length);
    assertEquals("END", Protocol.stringOf(byteBuffers[0]));
    assertEquals("END", Protocol.stringOf(byteBuffers[1]));
    assertEquals("END", Protocol.stringOf(byteBuffers[2]));
    assertEquals("END", Protocol.stringOf(byteBuffers[3]));
    assertEquals("END", Protocol.stringOf(byteBuffers[4]));
    assertEquals("END", Protocol.stringOf(byteBuffers[5]));

  }

  /**
   * Simulate a get simple request .
   */
  @Test
  public void testSimpleGetHeader()
      throws IOException {
    SESSION.queueRequest(bufferOf("get keyone\r\n"));
    SESSION.processRequests();
    Mockito.verify(CACHE, times(1)).get(anySet());
    Mockito.verify(SESSION, times(1)).onResponse(any(ByteBuffer[].class));
    ByteBuffer[] byteBuffers = SESSION.processResponses();
    // Nothing in the cache
    assertEquals(1, byteBuffers.length);
    assertEquals("END", Protocol.stringOf(byteBuffers[0]));
  }


  @Test
  public void testSetBytes()
      throws IOException {
    SESSION.queueRequest(bufferOf("set xy2key 0 0 12\r\nabcde\r\nfabcd\r\n"));
    SESSION.processRequests();
    Mockito.verify(CACHE, times(1)).set(any(CacheEntry.class));
    Mockito.verify(SESSION, times(1)).onResponse(any(ByteBuffer[].class));
    ByteBuffer[] byteBuffers = SESSION.processResponses();

    assertEquals(1, byteBuffers.length);
    assertEquals("STORED", stringOf(byteBuffers[0]));

    SESSION.queueRequest(bufferOf("get xy2key\r\n"));
    SESSION.processRequests();
    // the datablock is in the last buffer (hopefully)
    byteBuffers = SESSION.processResponses();

    assertEquals(7, byteBuffers.length);
    assertEquals("VALUE", stringOf(byteBuffers[0]));
    // key
    assertEquals("xy2key", stringOf(byteBuffers[1]));
    // flags
    assertEquals("0", stringOf(byteBuffers[2]));
    // num bytes
    assertEquals("12", stringOf(byteBuffers[3]));
    //cas
    assertEquals("0", stringOf(byteBuffers[4]));
    assertEquals("abcde\r\nfabcd", stringOf(byteBuffers[5]));
    assertEquals("END", stringOf(byteBuffers[6]));
  }

  @Test
  public void testSet()
      throws IOException {
    SESSION.queueRequest(bufferOf("set xy2key 0 0 10\r\nabcdefabcd\r\n"));
    SESSION.processRequests();
    Mockito.verify(CACHE, times(1)).set(any(CacheEntry.class));
    Mockito.verify(SESSION, times(1)).onResponse(any(ByteBuffer[].class));
    ByteBuffer[] byteBuffers = SESSION.processResponses();

    assertEquals(1, byteBuffers.length);
    assertEquals("STORED", stringOf(byteBuffers[0]));

    SESSION.queueRequest(bufferOf("get xy2key\r\n"));
    SESSION.processRequests();
    // the datablock is in the last buffer (hopefully)
    byteBuffers = SESSION.processResponses();

    assertEquals(7, byteBuffers.length);
    assertEquals("VALUE", stringOf(byteBuffers[0]));
    // key
    assertEquals("xy2key", stringOf(byteBuffers[1]));
    // flags
    assertEquals("0", stringOf(byteBuffers[2]));
    // num bytes
    assertEquals("10", stringOf(byteBuffers[3]));
    //cas
    assertEquals("0", stringOf(byteBuffers[4]));
    assertEquals("abcdefabcd", stringOf(byteBuffers[5]));
    assertEquals("END", stringOf(byteBuffers[6]));

    SESSION.queueRequest(bufferOf("set xykey 0 0 10\r\nabcdefabcd\r\n"));
    SESSION.processRequests();

    Mockito.verify(CACHE, times(2)).set(any(CacheEntry.class));
    Mockito.verify(SESSION, times(2)).onResponse(any(ByteBuffer[].class));
    byteBuffers = SESSION.processResponses();

    assertEquals(1, byteBuffers.length);
    assertEquals("STORED", stringOf(byteBuffers[0]));
    assertEquals(2, CACHE.size());

    SESSION.queueRequest(bufferOf("get xy2key xykey\r\n"));
    SESSION.processRequests();

    // the datablock is in the last buffer (hopefully)
    byteBuffers = SESSION.processResponses();

    assertEquals(13, byteBuffers.length);
    assertEquals("VALUE", stringOf(byteBuffers[0]));
    // key
    assertEquals("xy2key", stringOf(byteBuffers[1]));
    // flags
    assertEquals("0", stringOf(byteBuffers[2]));
    // num bytes
    assertEquals("10", stringOf(byteBuffers[3]));
    //cas
    assertEquals("0", stringOf(byteBuffers[4]));
    assertEquals("abcdefabcd", stringOf(byteBuffers[5]));

    assertEquals("VALUE", stringOf(byteBuffers[6]));
    // key
    assertEquals("xykey", stringOf(byteBuffers[7]));
    // flags
    assertEquals("0", stringOf(byteBuffers[8]));
    // num bytes
    assertEquals("10", stringOf(byteBuffers[9]));
    //cas
    assertEquals("0", stringOf(byteBuffers[10]));
    assertEquals("abcdefabcd", stringOf(byteBuffers[11]));
    assertEquals("END", stringOf(byteBuffers[12]));
  }

  /**
   * Test that the session can assemble a set from multiple buffers
   */
  @Test
  public void testFragmentedSet()
      throws IOException {
    SESSION.queueRequest(bufferOf("set xy2key 0 0 50\r\nabcdefabcd"));
    SESSION.queueRequest(bufferOf("abcdefabcd"));
    SESSION.queueRequest(bufferOf("abcdefabcd"));
    SESSION.queueRequest(bufferOf("abcdefabcd"));
    SESSION.queueRequest(bufferOf("abcdefabcd\r\n"));
    SESSION.processRequests();

    Mockito.verify(CACHE, times(1)).set(any(CacheEntry.class));
    Mockito.verify(SESSION, times(1)).onResponse(any(ByteBuffer[].class));
    ByteBuffer[] byteBuffers = SESSION.processResponses();
    assertEquals(1, byteBuffers.length);
    assertEquals("STORED", stringOf(byteBuffers[0]));

    SESSION.queueRequest(bufferOf("get xy2key\r\n"));
    SESSION.processRequests();
    // the datablock is in the last buffer (hopefully)
    byteBuffers = SESSION.processResponses();
    assertEquals(7, byteBuffers.length);
    assertEquals("VALUE", stringOf(byteBuffers[0]));
    // key
    assertEquals("xy2key", stringOf(byteBuffers[1]));
    // flags
    assertEquals("0", stringOf(byteBuffers[2]));
    // num bytes
    assertEquals("50", stringOf(byteBuffers[3]));
    //cas
    assertEquals("0", stringOf(byteBuffers[4]));
    assertEquals("abcdefabcdabcdefabcdabcdefabcdabcdefabcdabcdefabcd", stringOf(byteBuffers[5]));
    assertEquals("END", stringOf(byteBuffers[6]));
  }

  @Test
  public void longSetPackedGet()
      throws IOException {
    String[] messages = new String[]{
        "set xy2key 0 0 1344\r\nabcdefabcdefabcdefabcdefabcdefabcdefabcdefababcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefa"
        + "bcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabc"
        + "defabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdef"
        + "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdef"
        + "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcd"
        + "efabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcd"
        + "efabcdefabcdefabcdefabcdefabcdefabcdefcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdef"
        + "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdef\r\nget xyzkey xy2key\r\n"};
    SESSION.queueRequest(bufferOf(messages[0]));
    SESSION.processRequests();

    Mockito.verify(CACHE, times(1)).set(any(CacheEntry.class));
    Mockito.verify(SESSION, times(1)).onResponse(any(ByteBuffer[].class));
    ByteBuffer[] byteBuffers = SESSION.processResponses();
    assertEquals(8, byteBuffers.length);
    assertEquals("STORED", stringOf(byteBuffers[0]));
    assertEquals("VALUE", stringOf(byteBuffers[1]));
    // key
    assertEquals("xy2key", stringOf(byteBuffers[2]));
    // flags
    assertEquals("0", stringOf(byteBuffers[3]));
    // num bytes
    assertEquals("1344", stringOf(byteBuffers[4]));
    //cas
    assertEquals("0", stringOf(byteBuffers[5]));
    assertEquals(1344, stringOf(byteBuffers[6]).length());
    assertEquals("END", stringOf(byteBuffers[7]));
  }
}
