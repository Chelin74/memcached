package com.company;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.ClosedChannelException;
import java.nio.file.Files;
import java.util.stream.Collectors;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static junit.framework.TestCase.fail;

/**
 * ServerIT integration types of test which assumes which uses the socket to connect to communicate.
 */
public class ServerIT {

  /**
   * Logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(ServerIT.class);
  private static Server server;

  @Test
  public void testSet()
      throws IOException, InterruptedException {
    Client client = new Client(11211);
    client.send("set xy2key 0 0 10\r\n"
                + "abcdefabcd\r\n");
    client.receive(s -> s.endsWith("STORED"));
    client.send("get xy2key\r\n");
    client.receive(s -> s.endsWith("END"));

    client.shutdown();
  }

  @Test
  public void testBigBody()
      throws IOException, InterruptedException {
    Client client = new Client(11211);
    client.send("set mykey 0 0 2688\r\n",
        "abcdefabcdefabcdefabcdefabcdefabcdefabcdefababcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefab"
        + "cdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdef"
        + "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcd"
        + "efabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabc"
        + "defabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefab"
        + "cdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefab"
        + "cdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefab"
        + "cdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabc"
        + "defabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefcdefabcdefabcdefabcdefa"
        + "bcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabc"
        + "defabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcde"
        + "fabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefababcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcde"
        + "fabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefa"
        + "bcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabc"
        + "defabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcde"
        + "fabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefa"
        + "bcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabc"
        + "defabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcde"
        + "fabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefa"
        + "bcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefcdefabcdefabcdefabcd"
        + "efabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcde"
        + "fabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdef"
        + "abcdefabcdefabcdefabcdefabcdef\r\n");
    client.receive(s -> s.endsWith("STORED"));
    client.send("get mykey\r\n");
    client.receive(s -> s.endsWith("END"));
    client.shutdown();
  }

  @Test
  public void testBadMessage() {
    try(BufferedReader reader = new BufferedReader(new InputStreamReader(ServerIT.class.getClassLoader().getResourceAsStream("badmessage")))) {
      String string = reader.lines().collect(Collectors.joining(""));
      LOG.info("Length of string {}", string.length());
      Client client = new Client(11211);
      client.send(string);
      client.receive(String::isEmpty);
    } catch (InterruptedException e) {
      LOG.error("Error", e);
      fail();
    } catch (IOException e) {
      LOG.error("Unexpected", e);
    }
  }

  @BeforeClass
  public static void startServer()
      throws IOException {
    server = new Server(11211);
    server.start();
  }

  @AfterClass
  public static void stopServer()
      throws IOException {
    server.stop();
  }

}
