package com.company;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Client is a real client implementation which can communicate with a server over a socket.
 * The client can send a bunch of buffers to the server and then read data until it meets some criteria
 */
public class Client {
  private static final Logger LOG = LoggerFactory.getLogger(Client.class);
  private final InetSocketAddress hostAddress;
  private final AsynchronousSocketChannel client;

  Client(int port)
      throws IOException, InterruptedException {
    client = AsynchronousSocketChannel.open();
    hostAddress = new InetSocketAddress("localhost", port);

    connect(client, hostAddress);
  }

  public void send(String... strings) {
    int max = 4096;
    ByteBuffer buffer = ByteBuffer.allocate(max);
    Arrays.stream(strings).forEach(s -> {
      int slen = s.length();
      int sremaining = slen;
      LOG.info("Request {}", s.substring(0, Math.min(64, slen)));
      byte[] bytes = s.getBytes();

      for (int i = 0, chunk = 0; sremaining > 0; i += Math.min(sremaining, max), chunk++) {
        buffer.clear();
        buffer.put(bytes, chunk * max, Math.min(sremaining, max));
        sremaining -= max;
        buffer.flip();
        Future result = client.write(buffer);

        while (!result.isDone()) {
          Thread.yield();
        }
        try {
          Object o = result.get();
        } catch (InterruptedException | ExecutionException e) {
          LOG.error("Unable to write", e);
          throw new RuntimeException(e);
        }
      }
    });
  }

  public void receive(Function<String, Boolean> until) {
    boolean done = false;

    while (!done) {
      ByteBuffer response = ByteBuffer.allocate(128);
      Future<Integer> read = client.read(response);
      while (!read.isDone()) {
        Thread.yield();
      }
      String string = new String(response.array()).trim();
      done = until.apply(string);
      LOG.info("Completed: {} Response: {} ", done, string);
    }
  }

  void shutdown()
      throws IOException {
    client.close();
  }

  private void connect(AsynchronousSocketChannel client, InetSocketAddress hostAddress)
      throws InterruptedException {
    LOG.info("Connecting to {}", hostAddress);
    Future future = client.connect(hostAddress);

    while (!future.isDone()) {
      //Sleep for a second
      LOG.info(".. still attempting");
      Thread.sleep(3000);
    }
  }

  public static void main(String[] args)
      throws IOException, ExecutionException, InterruptedException {

    Client client = new Client(11211);

    String[] messages = new String[]{
        "set xy2key 0 0 1344\r\nabcdefabcdefabcdefabcdefabcdefabcdefabcdefababcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefa"
        + "bcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabc"
        + "defabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdef"
        + "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdef"
        + "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcd"
        + "efabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcd"
        + "efabcdefabcdefabcdefabcdefabcdefabcdefcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdef"
        + "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdef\r\n",
        "get xyzkey xy2key\r\n"};

    client.send(messages);
    client.receive(s -> s.endsWith("STORED"));
    client.receive(s -> s.endsWith("END"));
    client.shutdown();
  }
}
