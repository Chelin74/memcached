package com.company;


import com.company.cache.Cache;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.Arrays;

/**
 * TestSession, a fake implementation of Session
 */
class TestSession
    extends Session {

  TestSession(Cache cache)
      throws IOException {
    super(null, null, cache);
  }

  @Override
  protected long onWriteResponse(ByteBuffer[] buffers)
      throws IOException {
    //simulate a write
    Arrays.stream(buffers).forEach(b -> b.position(b.limit()));
    return 0;
  }

  /**
   * This is kind of analogous to how the socket write happens
   * not identical but fine enough for testing purposes
   *
   * @return
   *
   * @throws IOException
   */
  ByteBuffer[] processResponses()
      throws IOException {
    ByteBuffer[] buffers = collectPendingResponses();
    onWriteResponse(buffers);
    drainResponseQueue();
    return buffers;
  }

  @Override
  protected void onSignalWrite() {
  }

  @Override
  protected void onSignalWriteDrained() {
  }
}
