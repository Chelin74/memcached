package com.company.cache;


import java.util.Collection;
import java.util.Set;

/**
 * Cache interface for some cache operations.
 */
public interface Cache {

  /**
   * Get the data for the specified key, the data is conveniently a ByteBuffer.
   * If no data is located EMPTY_BUFFER will be returned.
   *
   * @param keys one ore more keys to retrieve
   *
   * @return the data or EMPTY_BUFFER
   */
  Collection<CacheEntry> get(Set<String> keys);

  /**
   * This is a limited implementation we ignore flags, exptime and noreply
   *
   * @param entry a {@link CacheEntry}
   *
   * @return true iff the data was added
   */
  boolean set(CacheEntry entry);

  /**
   * Number of entries in the cache
   *
   * @return the number of entries in the cache
   */
  int size();

  /**
   * Remove all entries from the cache.
   */
  void clear();
}
