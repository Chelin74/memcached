package com.company.cache;


import java.nio.ByteBuffer;

/**
 * CacheEntry is an the storage representation of an entry in the Memcached.
 * The cache entry is an immutable object.
 */
public final class CacheEntry {
  /**
   * A text string which should uniquely identify the data for clients
   * that are interested in storing and retrieving it.  Currently the
   * length limit of a key is set at 250 characters (of course, normally
   * clients wouldn't need to use such long keys); the key must not include
   * control characters or whitespace.
   */
  private final String key;

  /**
   * An arbitrary 16-bit unsigned integer (written out in
   * decimal) that the server stores along with the data and sends back
   * when the item is retrieved. Clients may use this as a bit field to
   * store data-specific information; this field is opaque to the server.
   * Note that in memcached 1.2.1 and higher, flags may be 32-bits, instead
   * of 16, but you might want to restrict yourself to 16 bits for
   * compatibility with older versions.
   * Since this is on the server side we will always accept 32 bit but will
   * only send 16 bits.
   */
  private final int flags;

  /**
   * Expiration time. If it's 0, the item never expires
   * (although it may be deleted from the cache to make place for other
   * items). If it's non-zero (either Unix time or offset in seconds from
   * current time), it is guaranteed that clients will not be able to
   * retrieve this item after the expiration time arrives (measured by
   * server time). If a negative value is given the item is immediately
   * expired.
   */
  private final long expiry;

  /**
   * The number of bytes in the data block to follow, *not*
   * including the delimiting \r\n. num_bytes may be zero (in which case
   * it's followed by an empty data block).
   */
  private final int numBytes;

  /**
   * Ignored.
   */
  private final long cas;

  /**
   * chunk of arbitrary 8-bit data of length {@link #numBytes}.
   */
  private final byte[] dataBlock;

  public CacheEntry(String key, int flags, long expiry, int numBytes, long cas, byte[] dataBlock) {
    this.key = key;
    this.flags = flags;
    this.expiry = expiry;
    this.numBytes = numBytes;
    this.cas = cas;
    this.dataBlock = dataBlock;

  }

  public long getCas() {
    return cas;
  }

  /**
   * Return a copy of the Datablock, the backing data is shared however.
   *
   * @return the Datablock as a {@link ByteBuffer}
   */
  public ByteBuffer getDataBlock() {
    ByteBuffer dup = ByteBuffer.wrap(dataBlock);
    dup.asReadOnlyBuffer();
    return dup;
  }

  long getExpiry() {
    return expiry;
  }

  public int getFlags() {
    return flags;
  }

  public String getKey() {
    return key;
  }

  public int getNumBytes() {
    return numBytes;
  }

  @Override
  public String toString() {
    return "CacheEntry{" +
           "key='" + key + '\'' +
           ", flags=" + flags +
           ", expiry=" + expiry +
           ", numBytes=" + numBytes +
           ", cas=" + cas +
           ", dataBlock=" + dataBlock.length +
           '}';
  }
}
