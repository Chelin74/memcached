package com.company.cache;


import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DefaultCache the default Cache implementation. This implementation is ephemeral
 * and restricted to memory, an assumption made hence the name Memcached.
 * Also the implementation is Threadsafe.
 */
public class DefaultCache
    implements Cache {

  /**
   * Logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(DefaultCache.class);

  /**
   * We store all the entries in-memory.
   */
  private final ConcurrentMap<String, CacheEntry> innerCache = new ConcurrentHashMap<>();

  @Override
  public Collection<CacheEntry> get(Set<String> key) {
    LOG.info("Getting cache entries for {}", key);
    return key.stream()
        .map(innerCache::get)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  @Override
  public boolean set(CacheEntry entry) {
    LOG.info("Inserting cache entry {}", entry);
    innerCache.put(entry.getKey(), entry);
    return true;
  }

  @Override
  public int size() {
    return innerCache.size();
  }

  @Override
  public void clear() {
    innerCache.clear();
  }
}
