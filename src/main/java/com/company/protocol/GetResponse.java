package com.company.protocol;


import com.company.cache.CacheEntry;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

/**
 * GetResponse.
 * <p>
 * Each item sent by the server looks like this:
 * <p>
 * VALUE&gt:key&lt:&gt:flags&lt:&gt:bytes&lt:[&gt:cas unique&lt:]\r\n
 * &gt:data block&lt:\r\n
 * <p>
 * -&gt:key&lt:is the key for the item being sent
 * <p>
 * -&gt:flags&lt:is the flags value set by the storage command
 * <p>
 * -&gt:bytes&lt:is the length of the data block to follow,*not*including
 * its delimiting \r\n
 * <p>
 * -&gt:cas unique&lt:is a unique 64-bit integer that uniquely identifies
 * this specific item.
 * <p>
 * -&gt:data block&lt:is the data for this item.
 * <p>
 * If some of the keys appearing in a retrieval request are not sent back
 * by the server in the item list this means that the server does not
 * hold items with such keys(because they were never stored,or stored
 * but deleted to make space for more items,or expired,or explicitly
 * deleted by a client).
 */
public class GetResponse
    implements Response {

  private final Collection<CacheEntry> cacheEntries;

  GetResponse(Collection<CacheEntry> cacheEntries) {
    this.cacheEntries = cacheEntries;
  }

  @Override
  public ByteBuffer[] response() {
    Stream<ByteBuffer> values = cacheEntries.stream().map(
        e -> new ByteBuffer[]{
            Protocol.bufferOf("VALUE "),
            Protocol.bufferOf(e.getKey() + " "),
            Protocol.bufferOf(String.valueOf(e.getFlags()) + " "),
            Protocol.bufferOf(String.valueOf(e.getNumBytes()) + " "),
            Protocol.bufferOf(String.valueOf(e.getCas()) + "\r\n"),
            e.getDataBlock()
        }
    ).flatMap(Arrays::stream);
    return Stream.concat(values, Stream.of(Protocol.bufferOf("END\r\n"))).toArray(ByteBuffer[]::new);
  }
}
