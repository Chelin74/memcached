// (c) Copyright (2017) Cloudera, Inc.
package com.company.protocol;


import java.nio.ByteBuffer;

/**
 * Response, base for all responses.
 */
public interface Response
  extends Protocol {

  /**
   * Get the {@link ByteBuffer buffers} representing this reponse.
   * The buffers is assumed to be {@link ByteBuffer#rewind() rewinded}.
   *
   * @return an array of {@link ByteBuffer buffers}
   */
  ByteBuffer[] response();
}
