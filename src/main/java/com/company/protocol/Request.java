package com.company.protocol;


import com.company.cache.Cache;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Request base for all requests.
 * <p>
 * It is assumed that {@link com.company.Session} will instantiate the appropriate {@link Request}.
 * The {@link Request} implementation occurs in 2 separate phases.
 * <ol>
 * <li>Header - Session reads a Request line and constructs the matching Request</li>
 * <li>Body - Session reads the next Request line and passes it to {@link #readBody(ByteBuffer)}</li>
 * </ol>
 * The Session is required to ensure that the entire Request has been parsed by checking
 * {@link #completed()}. If it returns true then Session should invoke {@link #process(Cache)},
 * to complete the request and return the resulting {@link Response}.
 */
public interface Request
    extends Protocol {
  /**
   * Read the content of the specified <tt>buffer</tt> as the body of this Request.
   *
   * @param buffer the buffer containing the entire or partial body
   *
   * @return true iff the entire body has been handleRequest
   *
   * @throws IOException
   */
  boolean readBody(ByteBuffer buffer)
      throws IOException;

  /**
   * Return true iff the entire {@link Request} has been handleRequest.
   *
   * @return true iff the {@link Request} has been handleRequest fully otherwise false
   */
  boolean completed();

  /**
   * Process the request on the specified <tt>cache</tt>
   *
   * @param cache the {@link Cache}
   *
   * @return the resulting {@link Response}
   */
  Response process(Cache cache)
      throws IOException;

  /**
   * If positive and non zero, the Session will skip rather than analyze line by line
   *
   * @return the body length or 0 iff no body exists or unknown
   */
  int bodyLength();
}
