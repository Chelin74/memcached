// (c) Copyright (2017) Cloudera, Inc.
package com.company.protocol;


import java.nio.ByteBuffer;

/**
 * Protocol a marker interface.
 *
 *
 */
public interface Protocol {

  static ByteBuffer bufferOf(String string) {
    return ByteBuffer.wrap(string.getBytes());
  }

  static String stringOf(ByteBuffer buffer) {
    buffer.rewind();
    byte[] bytes = new byte[buffer.remaining()];
    buffer.get(bytes);
    return new String(bytes).trim();
  }
}
