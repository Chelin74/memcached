package com.company.protocol;


import com.company.cache.Cache;
import com.company.cache.CacheEntry;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.stream.Collectors.toSet;

/**
 * GetRequest.
 * The retrieval commands "get" and "gets" operates like this:
 * <p>
 * get &gt;key&lt;*\r\n
 * gets &gt;key&lt;*\r\n
 * <p>
 * - &gt;key&lt;* means one or more key strings separated by whitespace.
 * <p>
 */
public class GetRequest
    implements Request {

  /**
   * Logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(GetRequest.class);
  private final Set<String> keys;

  /**
   * A get request can have one or more keys requested. We create a set
   * of these keys to be processed.
   *
   * @param header an iterator of one ore more keys that are being requested
   */
  public GetRequest(Iterator<String> header) {
    keys = StreamSupport
        .stream(Spliterators.spliteratorUnknownSize(header, Spliterator.ORDERED), false)
        .collect(toSet());
  }

  @Override
  public boolean readBody(ByteBuffer buffer)
      throws IOException {
      // get has no body
    return true;
  }

  @Override
  public boolean completed() {
    return true;
  }

  @Override
  public Response process(Cache cache) {
    LOG.debug("Processing Keys {}", keys);
    Collection<CacheEntry> cacheEntries = cache.get(keys);
    LOG.debug("Cache returned #{} entries", cacheEntries);
    return new GetResponse(cacheEntries);
  }

  @Override
  public int bodyLength() {
    return 0;
  }
}
