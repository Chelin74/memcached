package com.company.protocol;


import java.nio.ByteBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * SetResponse.
 * After sending the command line and the data block the client awaits
 * the reply, which may be:
 * <p>
 * - "STORED\r\n", to indicate success.
 * <p>
 * - "NOT_STORED\r\n" to indicate the data was not stored, but not
 * because of an error. This normally means that the
 * condition for an "add" or a "replace" command wasn't met.
 * <p>
 * - "EXISTS\r\n" to indicate that the item you are trying to store with
 * a "cas" command has been modified since you last fetched it.
 * <p>
 * - "NOT_FOUND\r\n" to indicate that the item you are trying to store
 * with a "cas" command did not exist.
 */
public class SetResponse
    implements Response {

  private final String response;

  SetResponse(String response) {
    this.response = response;
  }

  @Override
  public ByteBuffer[] response() {
    return new ByteBuffer[]{
        Protocol.bufferOf(response + "\r\n"),
        };
  }
}
