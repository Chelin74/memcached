package com.company.protocol;


import com.company.cache.Cache;
import com.company.cache.CacheEntry;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * SetRequest:
 * <p>
 * First, the client sends a command line which looks like this:
 * <p>
 * &lt;command name&gt; &lt;key&gt; &lt;flags&gt; &lt;exptime&gt; &lt;bytes&gt; [noreply]\r\n
 * cas &lt;key&gt; &lt;flags&gt; &lt;exptime&gt; &lt;bytes&gt; &lt;cas unique&gt; [noreply]\r\n
 * <p>
 * - &lt;command name&gt; is "set", "add", "replace", "append" or "prepend"
 * <p>
 * "set" means "store this data".
 * <p>
 * "add" means "store this data, but only if the server *doesn't* already
 * hold data for this key".
 * <p>
 * "replace" means "store this data, but only if the server *does*
 * already hold data for this key".
 * <p>
 * "append" means "add this data to an existing key after existing data".
 * <p>
 * "prepend" means "add this data to an existing key before existing data".
 * <p>
 * The append and prepend commands do not accept flags or exptime.
 * They update existing data portions, and ignore new flag and exptime
 * settings.
 * <p>
 * "cas" is a check and set operation which means "store this data but
 * only if no one else has updated since I last fetched it."
 * <p>
 * - &lt;key&gt; is the key under which the client asks to store the data
 * <p>
 * - &lt;flags&gt; is an arbitrary 16-bit unsigned integer (written out in
 * decimal) that the server stores along with the data and sends back
 * when the item is retrieved. Clients may use this as a bit field to
 * store data-specific information; this field is opaque to the server.
 * Note that in memcached 1.2.1 and higher, flags may be 32-bits, instead
 * of 16, but you might want to restrict yourself to 16 bits for
 * compatibility with older versions.
 * <p>
 * - &lt;exptime&gt; is expiration time. If it's 0, the item never expires
 * (although it may be deleted from the cache to make place for other
 * items). If it's non-zero (either Unix time or offset in seconds from
 * current time), it is guaranteed that clients will not be able to
 * retrieve this item after the expiration time arrives (measured by
 * server time). If a negative value is given the item is immediately
 * expired.
 * <p>
 * - &lt;bytes&gt; is the number of bytes in the data block to follow, *not*
 * including the delimiting \r\n. &lt;bytes&gt; may be zero (in which case
 * it's followed by an empty data block).
 * <p>
 * - &lt;cas unique&gt; is a unique 64-bit value of an existing entry.
 * Clients should use the value returned from the "gets" command
 * when issuing "cas" updates.
 * <p>
 * - "noreply" optional parameter instructs the server to not send the
 * reply.  NOTE: if the request line is malformed, the server can't
 * parse "noreply" option reliably.  In this case it may send the error
 * to the client, and not reading it on the client side will break
 * things.  Client should construct only valid requests.
 * <p>
 * After this line, the client sends the data block:
 * <p>
 * &lt;data block&gt;\r\n
 * <p>
 * - &lt;data block&gt; is a chunk of arbitrary 8-bit data of length &lt;bytes&gt;
 * from the previous line.
 * <p>
 * After sending the command line and the data block the client awaits
 * the reply, which may be:
 * <p>
 * - "STORED\r\n", to indicate success.
 * <p>
 * - "NOT_STORED\r\n" to indicate the data was not stored, but not
 * because of an error. This normally means that the
 * condition for an "add" or a "replace" command wasn't met.
 * <p>
 * - "EXISTS\r\n" to indicate that the item you are trying to store with
 * a "cas" command has been modified since you last fetched it.
 * <p>
 * - "NOT_FOUND\r\n" to indicate that the item you are trying to store
 * with a "cas" command did not exist.
 */
public class SetRequest
    implements Request {

  private static final Logger LOG = LoggerFactory.getLogger(SetRequest.class);

  /**
   * A key
   * is a text string which should uniquely identify the data for clients
   * that are interested in storing and retrieving it.  Currently the
   * length limit of a key is set at 250 characters (of course, normally
   * clients wouldn't need to use such long keys); the key must not include
   * control characters or whitespace.
   */
  private final String key;

  /**
   * &lt;flags&gt; is an arbitrary 16-bit unsigned integer (written out in
   * decimal) that the server stores along with the data and sends back
   * when the item is retrieved. Clients may use this as a bit field to
   * store data-specific information; this field is opaque to the server.
   * Note that in memcached 1.2.1 and higher, flags may be 32-bits, instead
   * of 16, but you might want to restrict yourself to 16 bits for
   * compatibility with older versions.
   * Since this is on the server side we will always accept 32 bit but will
   * only send 16 bits.
   */
  private final int flags;

  /**
   * expiration time. If it's 0, the item never expires
   * (although it may be deleted from the cache to make place for other
   * items). If it's non-zero (either Unix time or offset in seconds from
   * current time), it is guaranteed that clients will not be able to
   * retrieve this item after the expiration time arrives (measured by
   * server time). If a negative value is given the item is immediately
   * expired.
   */
  private final long expiry;

  /**
   * The number of bytes in the data block to follow, *not*
   * including the delimiting \r\n. num_bytes may be zero (in which case
   * it's followed by an empty data block).
   */
  private final int numBytes;

  /**
   * Ignored.
   */
  private final long cas;

  /**
   * "noreply" instructs the server to not send the
   * reply.  NOTE: if the request line is malformed, the server can't
   * parse "noreply" option reliably.  In this case it may send the error
   * to the client, and not reading it on the client side will break
   * things.  Client should construct only valid requests.
   */
  private final String noReply;

  /**
   * chunk of arbitrary 8-bit data of length {@link #numBytes}. This block includes the 2 eol bytes
   */
  private byte[] dataBlock;

  /**
   * Number of bytes outstanding for completeness
   */
  private transient int leftToRead;

  public SetRequest(Iterator<String> arguments) {
    key = arguments.next();
    flags = Integer.valueOf(arguments.next());
    expiry = Long.valueOf(arguments.next());
    numBytes = Integer.valueOf(arguments.next());
    if (arguments.hasNext()) {
      cas = Long.valueOf(arguments.next());
      if (arguments.hasNext()) {
        noReply = arguments.next();
      } else {
        noReply = null;
      }
      assert (!arguments.hasNext());
    } else {
      cas = 0L;
      noReply = null;
    }
  }

  @Override
  public boolean readBody(ByteBuffer buffer)
      throws IOException {
    dataBlock = new byte[numBytes + 2];
    buffer.get(dataBlock);
    return buffer.remaining() == 0;
  }

  @Override
  public boolean completed() {
    return dataBlock != null;
  }

  @Override
  public Response process(Cache cache)
      throws IOException {
    CacheEntry cacheEntry = toCacheEntry();
    LOG.info("Cache entry {} ", cacheEntry);
    return new SetResponse(cache.set(cacheEntry) ? "STORED" : "NOT_STORED");
  }

  @Override
  public int bodyLength() {
    return numBytes + 2;
  }

  // Convert this storage command into an immutable CacheEntry
  private CacheEntry toCacheEntry() {
    return new CacheEntry(key, flags, expiry, numBytes, cas, dataBlock);
  }
}
