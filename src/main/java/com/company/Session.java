package com.company;


import com.company.cache.Cache;
import com.company.protocol.GetRequest;
import com.company.protocol.Request;
import com.company.protocol.SetRequest;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Session is a per connection handler which processes each request in the order
 * of how they are received from the corresponding client.
 * Typically a client would concatenate one or more {@link ByteBuffer buffers}.
 * <p>
 * As the {@link #processRequests} is invoked zero or more {@link ByteBuffer buffers}
 * could be appended to the outgoing response queue.
 * <p>
 * The assumption is that each client appends and triggers the request flow
 * using the {@link #handleRequest()}. If the session wants to respond to
 * the client if appends the responses and then signal the selection key
 * that data is ready for writing. The selection thread should then call
 * {@link #handleResponse()} that keeps the channel open for writing as
 * long as the response queue has not been drained.
 * <p>
 * Some room for improvements:
 * - Buffer pooling
 */
public class Session {
  private static final Logger LOG = LoggerFactory.getLogger(Session.class);

  private static final ExecutorService executorService = new ThreadPoolExecutor(
      Runtime.getRuntime().availableProcessors(),
      Runtime.getRuntime().availableProcessors() * 2,
      30,
      TimeUnit.SECONDS,
      new LinkedBlockingQueue<>(2048));
  /**
   * The max size of a message ~ 2 MB?
   */
  private static final int MAX_MESSAGE_SIZE = 2 * 1024 * 1024;
  /**
   * An updater for the execution state for when we need to CAS the state
   */
  private static final AtomicIntegerFieldUpdater<Session> exectutionStateUpdater = AtomicIntegerFieldUpdater
      .newUpdater(Session.class, "executionState");
  ;
  /**
   * The MTU size for this session.
   */
  private final int mtu;
  private final SocketChannel client;
  private final Selector selector;
  private final Cache cache;
  /**
   * A queue of pending request buffers
   */
  private final BlockingDeque<ByteBuffer> requests;
  /**
   * A queue of pending response buffers
   */
  private final BlockingDeque<ByteBuffer> responses;
  /**
   * When a request spans more than a single {@link ByteBuffer} each
   * of the constituents will be added in order to partialRequestBuffers.
   * When a request has completed partialRequestBuffers is empty.
   */
  private final Queue<ByteBuffer> partialRequestBuffers = new ArrayDeque<>();
  /**
   * The current state of this session
   */
  private State state;
  /**
   * The next incomplete request line
   */
  private Request currentRequest;
  /**
   * The next complete request line.
   */
  private ByteBuffer nextRequestBuffer;
  /**
   * If not null this contains a partial request line;
   */
  private ByteBuffer pendingRequestBuffer;
  /**
   * The Sessions current {@link ExecutionState}
   */
  private volatile int executionState;

  /**
   * A tracker of the number of total bytes received
   */
  private long totalBytesReceived = 0;
  /**
   * This field is non 0 when a request spans more than a single
   * {@link ByteBuffer} and contains the total number of bytes
   * the request currently consists of. When a request has
   * been completely read then outstandingRequestBytes will
   * be reset to 0.
   */
  private int outstandingRequestBytes;
  /**
   * Instructs the request process to skip requestBytesToSkip bytes
   * this means that instead of looking for a \r\n combination in the
   * current request and instead forward each constituent {@link ByteBuffer}
   * until requestBytesToSkip is 0.
   */
  private int requestBytesToSkip;

  Session(Selector selector, SocketChannel client, Cache cache) {
    this.selector = selector;
    this.client = client;
    this.state = State.HEADER;
    this.cache = cache;
    this.executionState = ExecutionState.NONE.ordinal();
    this.requests = new LinkedBlockingDeque<>();
    this.responses = new LinkedBlockingDeque<>();

    int tmpMtu;
    if (client == null) {
      tmpMtu = 256;
    } else {
      try {
        tmpMtu = NetworkInterface.getByInetAddress(((InetSocketAddress) client.getLocalAddress()).getAddress())
            .getMTU();
      } catch (IOException e) {
        tmpMtu = 1024;
      }
    }
    mtu = tmpMtu;
  }

  /**
   * Signal that there are data to be sent.
   */
  protected void onSignalWrite() {
    enableOp(SelectionKey.OP_WRITE);
  }

  /**
   * Signal that no more response is pending.
   */
  protected void onSignalWriteDrained() {
    diableOp(SelectionKey.OP_WRITE);
  }

  /**
   * Called when there are responses which needs to be sent to
   * the client.
   *
   * @param buffers the {@link ByteBuffer buffers} to send
   *
   * @return the number of bytes transmitted
   *
   * @throws IOException
   */
  protected long onWriteResponse(ByteBuffer[] buffers)
      throws IOException {
    return client.write(buffers);
  }

  /**
   * Check if there are any pending requests.
   * Note: this method is not thread safe.
   *
   * @return true if there are more requests
   */
  boolean hasNextRequestBuffer()
      throws IOException {
    return (nextRequestBuffer = readRequestLine()) != null;
  }

  /**
   * Return the next request line, caller is supposed to check
   * {@link #hasNextRequestBuffer()} before calling this method.
   *
   * @return the next request line as a {@link ByteBuffer}
   */
  ByteBuffer nextRequestBuffer() {
    if (nextRequestBuffer == null) {
      throw new IllegalStateException("No more pending requests");
    }

    return nextRequestBuffer;
  }

  /**
   * Called when a Request has finished processing
   *
   * @param response one or more {@link ByteBuffer} that should be sent to the client
   *
   * @throws IOException
   */
  void onResponse(ByteBuffer... response)
      throws IOException {
    Arrays.stream(response).forEach(responses::offer);
    onSignalWrite();
  }

  /**
   * Handle each request. A {@link Request} is made up of two parts an header or epilogue and
   * a body or prologue. One session instance can only process a single {@link Request} from
   * a corresponding end-point, thus we will be running the session like a sort of FSM.
   * Only one thread should be allowed to call this as it modifies state, therefore this
   * method is synchronized on the {@link Session} instance itself.
   * *
   *
   * @throws IOException
   */
  void processRequests()
      throws IOException {
    while (hasNextRequestBuffer()) {
      ByteBuffer buffer = nextRequestBuffer();

      switch (state) {
        case HEADER:
          onHeader(buffer);
          if (currentRequest != null) {
            // in the case that the request knows the length we should fast forward to that location
            skipNextRequestBytes(currentRequest.bodyLength());
          }
          break;
        // intentional fall through: there may be a body left in the buffer
        case BODY:
          currentRequest.readBody(buffer);
          break;
      }

      if (currentRequest != null && currentRequest.completed()) {
        onResponse(currentRequest.process(cache).response());
        // ready to receive next request
        reset();
      }
    }
  }

  /**
   * Called when the selection key has something that needs to be read.
   *
   * @throws IOException
   */
  void handleRequest()
      throws IOException {

    ByteBuffer buffer = ByteBuffer.allocate(mtu);
    int read = client.read(buffer);
    totalBytesReceived += Math.max(0, read);
    LOG.debug("Read {} total {}", read, totalBytesReceived);
    buffer.flip();

    if (read == -1) {
      onSessionEnd(null);
    } else {
      queueRequest(buffer);

      if (exectutionStateUpdater
          .compareAndSet(this, ExecutionState.NONE.ordinal(), ExecutionState.ENQUEUED.ordinal())) {
        executorService.execute(() -> {
          while (!requests.isEmpty()) {
            try {
              executionState = ExecutionState.RUNNING.ordinal();
              this.processRequests();

              if (executionState == ExecutionState.RUNNING.ordinal()) {
                executionState = ExecutionState.NONE.ordinal();
              }
            } catch (IOException e) {
              LOG.error("Error ", e);
            }
          }
        });
      }
    }
  }

  /**
   * Called when the selection key is ready to send data to the client.
   *
   * @return the number of bytes written to the client
   *
   * @throws IOException
   */
  long handleResponse()
      throws IOException {
    if (responses.isEmpty()) {
      onSignalWriteDrained();
      return 0;
    } else {
      long write = onWriteResponse(collectPendingResponses());
      if (write == -1) {
        onSessionEnd(null);
        return -1;
      } else {
        LOG.debug("Wrote {}", write);
        drainResponseQueue();
        return write;
      }
    }
  }

  /**
   * Remove all responses which has been fully sent to the client
   */
  void drainResponseQueue() {
    ByteBuffer response;
    while ((response = responses.peek()) != null) {
      if (response.hasRemaining()) {
        return;
      } else {
        responses.poll();
      }
    }
  }

  /**
   * Adds a new request to the end of the queue.
   *
   * @param buffer the requests {@link ByteBuffer}
   */
  void queueRequest(ByteBuffer buffer) {
    // only queue new requests if the session is not in the
    // processes of terminating
    if (executionState < ExecutionState.STOPPING.ordinal()) {
      requests.offer(buffer);
    }
  }

  /**
   * Get the number of outstanding requests
   *
   * @return the number of outstanding requests
   */
  int requestQueueSize() {
    return requests.size();
  }

  ByteBuffer[] collectPendingResponses() {
    return responses.stream().toArray(ByteBuffer[]::new);
  }

  /**
   * Attempt to read the next request line. If none can be constructed
   * return null.
   *
   * @return the next request line {@link ByteBuffer} or null
   */
  ByteBuffer readRequestLine()
      throws IOException {
    return requests.isEmpty() ? null : attemptNextRequest();
  }

  /**
   * In the event that the request knows ahead of time how many bytes to receive as
   * body. Use this method to avoid checking every single byte.
   *
   * @param bytes the number of bytes to requestBytesToSkip
   */
  void skipNextRequestBytes(int bytes) {
    requestBytesToSkip = bytes;
  }

  /**
   * Attempt to read a complete line from {@link #requests}. Iterate through all of
   * them until a line is assembled or none could not. In the latter case all
   * progress is stored in the {@link #pendingRequestBuffer}. So that the
   * progress could be assumed from that point.
   * If a line is completely read then as a side effect {@link #pendingRequestBuffer}
   * is cleared.
   *
   * @return a {@link ByteBuffer}
   */
  private ByteBuffer attemptNextRequest()
      throws IOException {
    byte last = 0, b;

    ByteBuffer current;
    while ((current = requests.poll()) != null) {
      if (requestBytesToSkip > 0) {
        // In this case we will attempt to skip ahead until
        // there is enough bytes ready to create a new ByteBuffer
        assert (state == State.BODY);
        if (current.remaining() < requestBytesToSkip) {
          partialRequestBuffers.offer(current);
        } else {
          current.position(requestBytesToSkip);
          ByteBuffer duplicate = current.duplicate();
          ByteBuffer slice = current.slice();
          requests.push(slice);
          duplicate.flip();
          current = duplicate.slice();
        }

        outstandingRequestBytes += current.remaining();
        requestBytesToSkip -= current.remaining();
        if (requestBytesToSkip == 0) {
          // All bytes has been read.
          return finalizeRequestBuffer(current);
        }
      } else {
        current.mark();
        while (current.hasRemaining()) {
          b = current.get();
          switch (b) {
            case '\n':
              if (last == '\r') {
                if (current.hasRemaining()) {
                  ByteBuffer duplicate = current.duplicate();
                  ByteBuffer slice = current.slice();
                  // put the remaining buffer first in the
                  // request queue
                  requests.push(slice);
                  duplicate.flip();
                  current = duplicate.slice();
                } else {
                  //current.reset();
                  current.flip();
                }

                return finalizeRequestBuffer(current);
              }
              // intentional missing break here!
            default:
              last = b;
          }
        }

        // there are no more bytes in current, reset and add it to the pending buffers
        current.reset();
        outstandingRequestBytes += current.remaining();
        if (state == State.HEADER && outstandingRequestBytes > MAX_MESSAGE_SIZE) {
          // trivial check, a header can only be 2Mb
          partialRequestBuffers.clear();
          onSessionEnd(String.format("CLIENT_ERROR Header to large: %s\r\n", outstandingRequestBytes));
        } else {
          partialRequestBuffers.offer(current);
        }
      }
    }
    return null;
  }

  /**
   * Create a {@link ByteBuffer} which represents the request line.
   * If there are any  {@link #pendingRequestBuffer} these will be appended to the
   * request line in the correct order.
   * Finally we complete request line from the specified {@link ByteBuffer current buffer}.
   * If the <tt>current</tt> buffer contains more than one request lines then slice out the
   * first line and then reinsert the remaining bytes back first of the request queue.
   *
   * @param current a {@link ByteBuffer} containing the complete or tail of a request line
   *
   * @return a {@link ByteBuffer} representing a complete request line
   */
  private ByteBuffer finalizeRequestBuffer(ByteBuffer current) {
    if (partialRequestBuffers.isEmpty()) {
      logRequest(current);
      return current;
    } else {
      partialRequestBuffers.offer(current);
      outstandingRequestBytes += current.remaining();

      ByteBuffer request = ByteBuffer.allocate(outstandingRequestBytes);
      ByteBuffer next;
      while ((next = partialRequestBuffers.poll()) != null) {
        request.put(next);
      }
      request.flip();
      // nothing pending
      outstandingRequestBytes = 0;
      logRequest(request);
      return request;
    }
  }

  /**
   * Logs the incoming request.
   *
   * @param request the request to log
   */
  private void logRequest(ByteBuffer request) {
    if (LOG.isDebugEnabled()) {
      String srq = new String(request.array(), request.arrayOffset(), request.remaining());
      LOG.debug("Request {}", srq.trim());
    }
  }

  /**
   * Enable the specified op ({@link SelectionKey#OP_WRITE} or {@link SelectionKey#OP_READ}.
   *
   * @param op the operation to enable.
   * @return the current {@link SelectionKey#interestOps(int)}
   */
  private int enableOp(int op) {
    SelectionKey key = client.keyFor(this.selector);
    int ops = key.interestOps() | op;
    LOG.debug("current ops {}", ops);
    key.interestOps(key.interestOps() | op);
    return ops;
  }

  /**
   * Disable the specified op ({@link SelectionKey#OP_WRITE} or {@link SelectionKey#OP_READ}.
   *
   * @param op the operation to disable.
   * @return the current {@link SelectionKey#interestOps(int)}
   */
  private int diableOp(int op) {
    SelectionKey key = client.keyFor(this.selector);
    int ops = key.interestOps() & ~op;
    LOG.debug("current ops {}", ops);
    key.interestOps(ops);
    return ops;
  }

  /**
   * Complete the current requests.
   */
  private void reset() {
    requestBytesToSkip = 0;
    assert (partialRequestBuffers.isEmpty());
    state = State.HEADER;
    currentRequest = null;
  }

  /**
   * Called when a complete message header has been handleRequest into the specified <tt>buffer</tt>.
   * The <tt>len</tt> parameter indicates the number of bytes in the header.
   * The selector thread will unmarshal the serialized request and process it.
   */
  private void onHeader(ByteBuffer header)
      throws IOException {
    // position should be just at the end of the header
    byte[] bytes = new byte[header.remaining()];
    // read the bytes
    header.get(bytes);
    String arg = new String(bytes).trim();

    if (!arg.isEmpty()) {
      LOG.debug("Read header '{}'", arg);
      Iterator<String> args = Arrays.asList(arg.split("\\s")).iterator();
      String request = args.next();

      switch (request) {
        case "set":
          currentRequest = new SetRequest(args);
          break;

        case "get":
          currentRequest = new GetRequest(args);
          break;

        default:
          LOG.error("Unable to parse header {}", request);
          return;
      }

      state = State.BODY;
    }
  }

  private void onSessionEnd(String reason)
      throws IOException {
    // stop accepting input
    if (exectutionStateUpdater.compareAndSet(this, ExecutionState.RUNNING.ordinal(),
        ExecutionState.STOPPING.ordinal())) {
      if (client.isOpen()) {
        requests.clear();
        // stop listening to new
        client.shutdownInput();

        if (reason != null) {
          onResponse(ByteBuffer.wrap(reason.getBytes()));
        }

        while (client.isOpen() && !responses.isEmpty()) {
          Thread.yield();
        }
        client.shutdownOutput();
      }
      reset();
      LOG.info("Session terminated");

      client.close();
      executionState = ExecutionState.STOPPED.ordinal();
    }
  }

  public enum ExecutionState {
    NONE, ENQUEUED, RUNNING, STOPPING, STOPPED
  }

  public enum State {
    HEADER, BODY
  }
}
