package com.company;


import com.company.cache.Cache;
import com.company.cache.DefaultCache;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Server

    implements Runnable {
  private static final Logger LOG = LoggerFactory.getLogger(Server.class);

  private final Selector selector;
  private final ServerSocketChannel channel;
  private final Cache cache;
  private final int port;
  private volatile boolean running;
  private Thread thread;

  Server(int port)
      throws IOException {
    this.port = port;
    this.selector = Selector.open();
    channel = ServerSocketChannel.open();
    channel.bind(new InetSocketAddress(port));
    channel.configureBlocking(false);
    channel.register(selector, SelectionKey.OP_ACCEPT);
    cache = new DefaultCache();
  }

  public void start() {
    thread = new Thread(this);
    running = true;
    LOG.info("Starting server listening to {}", port);
    thread.start();
  }

  public void stop()
      throws IOException {
    running = false;
    channel.close();
    LOG.info("Stopping server");
  }

  @Override
  public void run() {
    SocketChannel client =  null;

    while (running) {
      try {
        selector.selectNow();
        Iterator<SelectionKey> keys = selector.selectedKeys().iterator();

        while (keys.hasNext()) {
          SelectionKey key = keys.next();
          keys.remove();

          if (key.isValid()) {
            if (key.isAcceptable()) {
              client = channel.accept();
              client.configureBlocking(false);
              client.register(selector, SelectionKey.OP_READ);
            }
            if (key.isReadable()) {
              client = (SocketChannel) key.channel();

              LOG.debug("Reading from {}", client.getRemoteAddress());
              Session session = (Session) key.attachment();
              if (session == null) {
                key.attach(session = new Session(selector, client, cache));
              }
              session.handleRequest();
            }
            if (key.isWritable()) {
              client = (SocketChannel) key.channel();
              if (!client.isOpen()) {
                continue;
              }

              LOG.debug("Writing to  {}", client.getRemoteAddress());
              Session session = (Session) key.attachment();
              assert (session != null);
              session.handleResponse();
            }
          }
        }
      } catch (IOException e) {
        if (client != null && client.isOpen()) {
          LOG.error("Problems", e);
        }
        // else this is since operation was interrupted by connection
        // termination
      }
    }
  }

  public static void main(String[] args)
      throws IOException, InterruptedException {
    Server server = new Server(11211);
    server.start();
    LOG.info("Press enter to terminate server");
    System.in.read();
    server.stop();
  }
}
