Memcached
=========

I should begin to state that I unfortunetaly did not have too much available time to investigate Memcached and how the protocol actually worked.
Still I'm a little bit confused about the description of the protocol it intermingles terms such as "16 bit unsigned integer" and "written out
as a decimal". However I assume that the lack of reference to any endian order that the text protocol is just that it serializes integers as
String representation.

I've not yet started so let's see where this ends up?

Phase I
-------
Decisions, ok I decided to write this in Java mostly because IO is always going to be the real bottleneck for these kinds of applications.

* I decided to use Non-blocking NIO, I think that this will reduce the number of blocked threads and thus increase throughput, it should
also have pretty good latency characteristics. If we optimized for latency Asynchronous NIO2 would be the best bet, however it effectivly
wastes a thread per connection when doing reading, which gets little bogus when it comes to scaling nicely.
* We could do somehting really fancy with creating thread groups which we pass on the selection key to, i.e. we have one selector which
accepts new clients, as soon as these finished connecting we then register the seleciton key with another selector thread. I think that
this is overkill
     - A single thread on a modern computer should have no problem pegging the NIC, certainly a 1GE and in many cases even a 10GE
* The other decsision I've made is to just have the selector thread queue requests and dequeue responses, all the other work of
deserializing/serializing a request/response could be handed out on a thread pool.
* The cache is a very thin implementation we could just delegate to a ConcurrentHashMap (CHM) and be done with it
    - The cache do not need any testing with this implementation, since it isn't doing anything really
* BufferPooling - I should say I do have a couple of Patents for Oracle in this particualar field so if I were to do a proper implementation
I would probably pool the ByteBuffers.
* MultiByteBuffer - I would like to have spent some time building out a 'MultiByteBuffer' which would have acted as a ByteBuffer ontop of a ByteBuffer array. I've done this before, but I think it is overkill right now. The side effect is that we in some cases, when we have requests that spans one or more requests is that we will do some unnecessary copying.

Session
------
Session is the centerpiece in this solution it gets attached to the channel of the client which it belongs to. That is how we keep the in-order guarantees for requests & responses.

Protocol
-------
At this point only Get and Set is implemented. For more detail see src/main/java/com/company/protocol

Cache
-----
The actual cache is implemented using a CHM since multiple Sessions could
be modifying and reading concurrently. However the values themselves are
immutable, ensuring that a Get will not get a 'hacked' value.

Server
-----
The server starts listening to a port the simplest way of starting the server
is after building ```mvn verify``` run the generated jar ```java -jar MemcachedNIO-1.0-SNAPSHOT-jar-with-dependencies.jar```.

example:

```cd target

~/memcached/target: (master) ☀ > java -jar MemcachedNIO-1.0-SNAPSHOT-jar-with-dependencies.jar 

[main] INFO com.company.Server - Starting server listening to 11211

[main] INFO com.company.Server - Press enter to terminate server
```

The server listens to new connections as soon as a connection has been accepted
the client can transmit data. The requests gets queued to the corresponding session
the session will then be scheduled on a thread pool for processing any pending 
requests, which also could include other arriving requests. The first processRequest()
will drain the incoming queue to its best abilities. Any subsequently scheduled processes for the same session will short circuit as there are no more requests
to process. 

Each request is handled in the order of how it arrives with respect to the Session
there are no ordering guarantees between Sessions. Responses will be ordered to 
the client in respect to the processed requests.

